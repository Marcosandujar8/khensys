﻿using Entrevista_khensysCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Entrevista_khensysCO.Controllers
{
    public class permisosController : Controller
    {
        private dataContext _db = new dataContext();
        // GET: permisos
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult listaPermisos()
        {
            return View(_db.TipoPermiso.ToList());
        }

        public ActionResult CrearTipoPermisos()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CrearTipoPermisos(TipoPermiso tipos)
        {
            if (tipos.Descripcion!=null)
            {
                _db.TipoPermiso.Add(tipos);
                _db.SaveChanges();
                return RedirectToAction("listaPermisos");
            }else return View();
        }

        public ActionResult listaPermisosSolicitados()
        {
            return View(_db.permisos.ToList());
        }

        public ActionResult crearPermisos()
        {
            ViewBag.TipoPermiso = new SelectList(_db.TipoPermiso.ToList(), "Id", "Descripcion");
            return View();
        }
        [HttpPost]
        public ActionResult crearPermisos(permisos permiso)
        {
            if (ModelState.IsValid)
            {
                _db.permisos.Add(permiso);
                _db.SaveChanges();
                return RedirectToAction("listaPermisosSolicitados");
            }
            else return RedirectToAction("crearPermisos");
        }
        public ActionResult eliminarPermiso(int Id)
        {
            var permiso = _db.permisos.Find(Id);
            _db.permisos.Remove(permiso);
            _db.SaveChanges();
            return RedirectToAction("listaPermisosSolicitados");
        }
    }
}