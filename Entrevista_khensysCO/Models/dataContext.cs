namespace Entrevista_khensysCO.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class dataContext : DbContext
    {
        public dataContext()
            : base("name=dataContext")
        {
        }

        public virtual DbSet<permisos> permisos { get; set; }
        public virtual DbSet<TipoPermiso> TipoPermiso { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<permisos>()
                .Property(e => e.NombreEmpleado)
                .IsUnicode(false);

            modelBuilder.Entity<permisos>()
                .Property(e => e.ApellidoEmpleado)
                .IsUnicode(false);

            modelBuilder.Entity<TipoPermiso>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<TipoPermiso>()
                .HasMany(e => e.permisos)
                .WithRequired(e => e.TipoPermiso1)
                .HasForeignKey(e => e.TipoPermiso)
                .WillCascadeOnDelete(false);
        }
    }
}
