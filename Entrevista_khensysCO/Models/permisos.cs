namespace Entrevista_khensysCO.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class permisos
    {
        public int Id { get; set; }

        [Required]
        [StringLength(25)]
        public string NombreEmpleado { get; set; }

        [Required]
        [StringLength(25)]
        public string ApellidoEmpleado { get; set; }

        public int TipoPermiso { get; set; }

        [Column(TypeName = "date")]
        public DateTime FechaPermiso { get; set; }

        public virtual TipoPermiso TipoPermiso1 { get; set; }
    }
}
